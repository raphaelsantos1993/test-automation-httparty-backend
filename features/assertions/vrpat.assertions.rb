require "rspec"

class Assertions
  include RSpec::Matchers

  # Método para verificar se a requisição foi bem-sucedida
  # Parâmetros:
  # - status_code: o código de status da resposta
  # - body: o corpo da resposta
  # - type_of_establishments: tipo de estabelecimentos esperado no corpo da resposta
  def request_success(status_code, body, type_of_establishments)
    expect(status_code).to eq(200) # Verifica se o código de status é 200
    expect(body).to include(type_of_establishments) # Verifica se o corpo da resposta inclui o tipo de estabelecimentos esperado
    puts status_code # Imprime o código de status
  end
end
