# Definição do módulo VRPAT para armazenar configurações da API VRPAT
module VRPAT
    include HTTParty # Inclui o módulo HTTParty para realizar solicitações HTTP
    #@token --> caso a api tenha autenticação
    base_uri 'https://portal.vr.com.br/api-web/comum' # Define o URI base da API
    format :json # Define o formato padrão das solicitações como JSON
    headers 'Content-Type': 'application/json' # Define o cabeçalho Content-Type para JSON
  end
  