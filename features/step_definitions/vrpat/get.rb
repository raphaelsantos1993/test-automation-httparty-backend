# Definindo o cenário onde o usuário consulta as informações no portal
Dado('que o usuario consulta as informações no portal') do
  @get_info = Vrpat_Requests.new # Inicializa uma nova instância da classe Vrpat_Requests para fazer a solicitação
  @assert = Assertions.new # Inicializa uma nova instância da classe Assertions para realizar as asserções
end

# Realizando a busca das informações
Quando('realiza a busca das informações') do
  @response = @get_info.find_info_vrpat # Faz a solicitação para obter as informações VRPAT
end

# Verificando se uma lista de estabelecimentos foi retornada
Entao('uma lista de estabelecimentos deve retorna') do
  @body = JSON.parse(@response.body) # Converte o corpo da resposta para um objeto JSON
  @message = 'typeOfEstablishment' # Define a mensagem a ser verificada nos testes
  
  @assert.request_success(@response.code, @body, @message) # Realiza as asserções para verificar se a requisição foi bem-sucedida
  
  type_of_establishments = @body['typeOfEstablishment'] # Obtém uma lista de tipos de estabelecimentos a partir do corpo da resposta
  random_type = type_of_establishments.sample # Seleciona aleatoriamente um tipo de estabelecimento da lista
  puts "Tipo de Estabelecimento Aleatório: #{random_type}"   # Imprime o tipo de estabelecimento aleatório
  puts "Nome do Estabelecimento Aleatório: #{random_type['name']}" # Imprime o nome do estabelecimento aleatório
end
