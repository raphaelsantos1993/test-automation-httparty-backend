# Projeto Automação HTTParty - Frontend

## Descrição

Este projeto com HTTParty foi desenvolvido para realizar testes automatizados no backend do portal da VR. O objetivo é validar o retorno do endpoint de consulta que fornece informações sobre os produtos e estabelecimentos da VR.

## Tecnologias Utilizadas

- [HTTParty](https://github.com/jnunemaker/httparty)
- [Ruby](https://www.ruby-lang.org/)

## Pré-requisitos

Antes de executar o projeto, certifique-se de ter as seguintes ferramentas instaladas em seu sistema:

- [Ruby](https://www.ruby-lang.org/): Ruby é necessário para executar o projeto. Você pode baixar e instalar a versão adequada para o seu sistema operacional a partir do site oficial.

  - **Windows:**
    - Baixe o instalador do RubyInstaller do [site oficial](https://rubyinstaller.org/downloads/).
    - Execute o instalador e siga as instruções na tela. Certifique-se de marcar a opção para adicionar o Ruby ao PATH durante a instalação.

  - **macOS:**
    - Instale o gerenciador de versões do Ruby chamado rbenv usando o Homebrew. No terminal, execute:
      ```bash
      brew install rbenv
      rbenv init
      ```
    - Siga as instruções para adicionar o rbenv ao seu shell. Isso geralmente envolve adicionar algumas linhas ao seu arquivo de configuração do shell, como `~/.bash_profile` ou `~/.zshrc`.
    - Instale o Ruby 3.0.0 usando rbenv:
      ```bash
      rbenv install 3.0.0
      rbenv global 3.0.0
      ```

  - **Linux:**
    - Instale o rbenv usando o gerenciador de pacotes do seu sistema (por exemplo, apt, yum, pacman). Consulte a documentação específica do seu sistema para obter instruções detalhadas.
    - Instale o Ruby 3.0.0 usando o rbenv:
      ```bash
      rbenv install 3.0.0
      rbenv global 3.0.0
      ```
     - Abra o seu arquivo de perfil do shell (por exemplo, ~/.bashrc, ~/.bash_profile, ~/.zshrc) em um editor de texto.

     - Adicione as seguintes linhas ao final do arquivo para configurar o rbenv e      adicionar o diretório das gems do Ruby ao seu PATH:

      ```bash
       export PATH="$HOME/.rbenv/bin:$PATH"
       eval "$(rbenv init -)"
       export PATH="$HOME/.rbenv/plugins/ruby-build/bin:$PATH"
      ```
     - Salve e feche o arquivo.
     
     - Recarregue o arquivo de perfil do shell para aplicar as alterações. No           terminal, execute:
     
     ```bash
     source ~/.bashrc   # Ou ~/.bash_profile, ou ~/.zshrc, dependendo do seu          shell
     ```
     - Verificação da instalação:

     - Abra um novo terminal.
     - Execute ruby -v para verificar se o Ruby foi instalado corretamente.
 
## Como Executar os Testes

### Passos
1. Clone este repositório para o seu ambiente local, execute o seguinte comando no terminal:
   ```bash
   git clone https://github.com/seu-usuario/nome-do-repositorio.git
    ```

2. Navegue até a pasta raiz do projeto. No terminal, utilize o comando:

  ```bash
  cd nome-do-repositorio
  ```

3. Instale as dependências do projeto com o Bundler. No terminal, execute o seguinte comando:

  ```bash
  bundle install
  ```

4. Execute os testes automatizados. No terminal, utilize o seguinte comando para iniciar os testes:

  ```bash
  cucumber
  ```
  
5. Opções do Comando Cucumber:
Para executar apenas cenários com uma determinada tag, adicione a opção --tags seguida pela tag desejada. Por exemplo, para executar apenas cenários com a tag @api, execute:

   ```bash
   cucumber --tags @api
   ```
   
6. Para gerar um relatório HTML com os resultados dos testes, adicione a opção --format html --out <caminho-do-arquivo>. Por exemplo, para gerar um relatório chamado report.html, execute:

   ```bash
   cucumber --format html --out report.html
   ```

7. Para executar os testes em modo verbose (com mais detalhes), adicione a opção -v ou --verbose. Por exemplo:

   ```bash
   cucumber -v
   ```
8. Para executar os testes em modo silencioso (sem saída detalhada), adicione a opção --quiet ou -q. Por exemplo:

   ```bash
   cucumber --quiet
   ```
 
 * O projeto está estruturado da seguinte forma:
   
 ```sql
 test-automation-frontend/
│
├── features/                          // Pasta principal dos cenários de teste
│   ├── assertions/                    // Pasta para asserções de teste
│   │   └── vrpat.assertions.rb       // Arquivo de asserções para o VRPAT
│   ├── bdd/                           // Pasta para cenários de teste BDD
│   │   └── get.feature                // Arquivo de feature para a operação GET
│   ├── requests/                      // Pasta para arquivos de requisições
│   │   └── vrpat.request.rb          // Arquivo para requisições ao VRPAT
│   ├── step_definitions/              // Pasta para definições de passos dos cenários de teste
│   │   └── vrpat/                     // Pasta para definições de passos do VRPAT
│   │       └── get.rb                 // Arquivo de definição de passos para a operação GET
│   └── support/                       // Pasta para arquivos de suporte aos testes
│       ├── base_url.rb               // Arquivo para definição da URL base (Módulo VRPAT para armazenar configurações da API VRPAT)
│       ├── database.rb               // Arquivo para armazenamento de massas de teste (Classe de armazenamento de massas de teste)
│       └── env.rb                    // Arquivo de configuração geral do ambiente de teste (imports das gems)
│
├── .gitignore                         // Arquivo de especificação de arquivos a serem ignorados pelo git
├── Gemfile                            // Arquivo de especificações de dependências Ruby
└── README.md                          // Arquivo README com informações sobre o projeto

 ```

## Estrutura do Projeto

1. **features/**
   - **assertions/**
     - `vrpat.assertions.rb`: Arquivo de asserções para o VRPAT
   - **bdd/**
     - `get.feature`: Arquivo de feature para a operação GET
   - **requests/**
     - `vrpat.request.rb`: Arquivo para requisições ao VRPAT
   - **step_definitions/**
     - **vrpat/**
       - `get.rb`: Arquivo de definição de passos para a operação GET
   - **support/**
     - `base_url.rb`: Arquivo para definição da URL base (Módulo VRPAT para armazenar configurações da API VRPAT)
     - `database.rb`: Arquivo para armazenamento de massas de teste (Classe de armazenamento de massas de teste)
     - `env.rb`: Arquivo de configuração geral do ambiente de teste (imports das gems)

2. **.gitignore**: Arquivo de especificação de arquivos a serem ignorados pelo git
3. **Gemfile**: Arquivo de especificações de dependências Ruby
4. **README.md**: Arquivo README com informações sobre o projeto


--------------------------------------------------------------------------------------------------------------------


## Cenários de Teste Ultilizado BDD:

```gherkin
Funcionalidade: Consulta informações no portal VR.

@get_info
Cenario: Busca informações produtos e estabelecimentos. 
Dado que o usuario consulta as informações no portal
Quando realiza a busca das informações 
Entao uma lista de estabelecimentos deve retorna
```
--------------------------------------------------------------------------------------------------------------------


